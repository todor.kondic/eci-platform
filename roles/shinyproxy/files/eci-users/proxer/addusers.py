#!/usr/bin/env python3
import yaml
import os

fn_passwd="eci_passwords.yml"
fn_application="application.in.yml"
fn_users="eci_users.yml"

fn_out="application.yml"
names = yaml.safe_load(fn_passwd)
with open(fn_passwd, 'r') as file:
    passwd = yaml.safe_load(file)['eci_passwords']

with open(fn_users,'r') as file:
    x = yaml.safe_load(file)
    users = x['eci_users']
    overseers = x['eci_overseers']
    all_users = list(set(users+overseers))
    
with open(fn_application,'r') as file:
    appln = yaml.safe_load(file)


def use_group(user):
    res=None
    if user in users:
        res = "eci"
    elif user in overseers:
        res = "overseers"
    return(res)

appln['proxy']['users']=[ { 'name':x,'groups':use_group(x),'password':passwd[x] } for x in all_users ]

with open(fn_out, 'w') as file:
    yaml.dump(appln, file, default_flow_style=False,sort_keys=False)


os.remove(fn_passwd)
os.remove(fn_users)
