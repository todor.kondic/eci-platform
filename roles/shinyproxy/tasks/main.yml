- name: Install docker.
  apt:
    name: [ 'docker.io',
            'nginx',
            'python3-yaml' ]


- name: Create service user 'proxer'.
  user:
    name: proxer
    system: yes
    groups:
      - docker
      - eci

- name: Create /eci-users/proxer.
  ansible.builtin.file:
    path: /eci-users/proxer
    owner: proxer
    state: directory
    mode: '0711'

- name: Copy TLS keys for use here (priv).
  ansible.builtin.copy:
    remote_src: yes
    src: /etc/eci-platform/priv/certificate.key
    dest: /eci-users/proxer/certificate.key
    owner: proxer
    mode: 0600

- name: Copy TLS keys for use here(pub).
  ansible.builtin.copy:
    remote_src: yes
    src: /etc/eci-platform/pub/certificate.key
    dest: /eci-users/proxer/certificate.pub.key
    owner: proxer
    mode: 0644

- name: Copy configuration file for nginx (shinyproxy).
  notify: update-www-conf
  ansible.builtin.copy:
    src: files/etc/nginx/conf.d/shinyproxy.conf
    dest: /etc/nginx/conf.d/shinyproxy.conf
    mode: '0644'

- name: Write the hostname/IP address to the shinyproxy.conf file.
  notify: update-www-conf
  ansible.builtin.replace:
    path: /etc/nginx/conf.d/shinyproxy.conf
    regexp: '___ECI_VM_HOSTNAME___'
    replace: "{{ hostvars[inventory_hostname]['ansible_default_ipv4']['address'] }}"
    
- name: Get Shinyproxy.
  get_url:
    url: https://www.shinyproxy.io/downloads/shinyproxy-2.6.0.jar
    checksum: "sha256:33e79a030294dc4dcb61c3030a53d239a964d6aa1122143d854a329c5c7a233b"
    dest: /eci-users/proxer/shinyproxy.jar
    owner: proxer
    mode: '0700'

- name: Get Shinyscreen docker container.
  get_url:
    url: https://zenodo.org/record/6362024/files/docker-shinyscreen.tar.gz?download=1
    dest: /eci-users/proxer/docker-shinyscreen.tar.gz
    checksum: "sha256:1c1ec4a14d3a691d1a79aaa43e8e58b36ccd5363a6b056c17a7177357dd31d06"
    owner: proxer
    mode: '0700'
    
- name: Create docker config directory.
  ansible.builtin.file:
    path: /etc/systemd/system/docker.service.d
    state: directory
    mode: '0744'
    
- name: Copy configuration file for docker (shinyproxy).
  notify:
    - docker-reconf
  ansible.builtin.copy:
    src: files/etc/systemd/system/docker.service.d/override.conf
    dest: /etc/systemd/system/docker.service.d/override.conf
    mode: '0644'

- name: Load docker image.
  shell: docker load -i docker-shinyscreen.tar.gz && touch loaded.shinyscreen
  args:
    creates: loaded.shinyscreen
    chdir: /eci-users/proxer


- name: Copy config file for shinyproxy.
  tags: shinyproxy-conf
  ansible.builtin.copy:
    src: files/eci-users/proxer/application.in.yml
    dest: /eci-users/proxer/application.in.yml
    owner: proxer
    mode: '0600'

- name: Copy users file for shinyproxy.
  tags: shinyproxy-conf
  ansible.builtin.template:
    src: "eci-users/proxer/{{ item }}.tmpl.yml"
    dest: "/eci-users/proxer/{{ item }}.yml"
    owner: proxer
    mode: '0600'
  loop:
    - eci_passwords
    - eci_users

- name: Generate application.yml.
  tags: shinyproxy-conf
  notify:
    - shinyproxy-application
  ansible.builtin.script:
    cmd: files/eci-users/proxer/addusers.py
    chdir: /eci-users/proxer
    executable: python3
    
- name: Copy shinyproxy script.
  tags: shinyproxy-conf
  notify: shinyproxy-service
  ansible.builtin.copy:
    src: eci-users/proxer/run_shinyproxy.sh
    dest: /eci-users/proxer/run_shinyproxy.sh
    owner: proxer
    mode: '0744'

- name: Copy service file for shinyproxy.
  tags: shinyproxy-conf
  notify: shinyproxy-service
  ansible.builtin.copy:
    src: files/etc/systemd/system/shinyproxy.service
    dest: /etc/systemd/system/shinyproxy.service
    mode: '0644'

